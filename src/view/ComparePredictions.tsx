import IfSet from "../component/IfSet";
import PredictFormComponent from "../component/PredictFormComponent";
import PredictResultComponent from "../component/PredictResultComponent";
import { usePredictOnAll } from "../hooks/modelHooks";

export default function ComparePredictions() {

	const { predictions, error, predict } = usePredictOnAll();

	const handlePredict = (f1: File, f2: File) => predict(f1, f2);

	return (
		<div className="container d-flex flex-column gap-3 my-3">
			<div className="row">
				<div className="col">
					<h1>Compare model outputs</h1>
				</div>
			</div>

			<IfSet var={error}>
				<div className="row">
					<div className="col">Error: {error}</div>
				</div>
			</IfSet>

			{predictions && <div className="row mt-3">
				<div className="col d-flex flex-column gap-2">
					{predictions.map(pred => <PredictResultComponent result={pred} />)}
				</div>
			</div>}

			<div className="row">
				<div className="col">
					<PredictFormComponent onPredict={handlePredict} />
				</div>
			</div>
		</div>
	);
}
