import { Link } from "react-router-dom";
import { Model } from "../domain/model";
import { useModelList } from "../hooks/modelHooks";

export default function ModelListView() {

	const { models } = useModelList();

	const modelTile = (model: Model) => (
		<Link
			key={model.name}
			to={`/models/${model.name}`}
			className="list-group-item list-group-item-action list-group-flush"
			aria-current="true">

			<div className="fw-bold fs-3">{ model.name }</div>

			<div>
				Trained on&nbsp;
				<span className="text-decoration-underline fw-bold">{ model.train_dataset }</span>
			</div>

			<div>
				Train: { model.train_loss } loss, { model.train_accuracy } accuracy
			</div>

			<div>
				Test: { model.test_loss } loss, { model.test_accuracy } accuracy
			</div>
		</Link>

	);

	return (
		<div className="container">
			<div className="row my-3">
				<h1>Available models</h1>
			</div>

			<div className="row">
				<div className="col">

					<ul className="list-group list-group-flush">
						{ models?.map(m => modelTile(m)) }
					</ul>

				</div>
			</div>
		</div>
	);
}
