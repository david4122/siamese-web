import { useParams } from "react-router-dom";
import PredictFormComponent from "../component/PredictFormComponent";
import { useModel, usePredict } from "../hooks/modelHooks";
import PredictResultComponent from "../component/PredictResultComponent";
import IfSet from "../component/IfSet";

export default function ModelView() {

	const params = useParams();
	const { model } = useModel(params.model);
	const { prediction, error, predict } = usePredict(params.model);

	const handlePredict = (first: File, second: File) => predict(first, second);

	return (
		<div className="container d-flex flex-column my-3">
			<div className="row">
				<div className="col">
					<h2 className="fw-bold">{model?.name}</h2>
				</div>
			</div>

			<div className="row my-1">
				<div className="col">
					Trained on&nbsp;
					<span className="text-decoration-underline fw-bold">
						{model?.train_dataset}
					</span>
				</div>
			</div>

			<div className="col">
				<div className="row">
					<div className="col">Train score:</div>
					<div className="col">{model?.train_loss} loss</div>
					<div className="col">{model?.train_accuracy} accuracy</div>
				</div>

				<div className="row">
					<div className="col">Test score:</div>
					<div className="col">{model?.test_loss} loss</div>
					<div className="col">{model?.test_accuracy} accuracy</div>
				</div>
			</div>

			<div className="row mt-3">
				<div className="col">
					<img className="w-100" alt="model graph" src={model?.model_graph} />
				</div>
			</div>

			<div className="row mt-3">
				<div className="col">
					<h3>Training</h3>
				</div>
			</div>

			<div className="row mt-1">
				<div className="col-12 col-md-6">
					<img className="w-100" alt="loss graph" src={model?.loss_graph} />
				</div>

				<div className="col-12 col-md-6">
					<img className="w-100" alt="accuracy graph" src={model?.accuracy_graph} />
				</div>
			</div>

			<div className="row mt-3">
				<div className="col">
					<h3>Test</h3>
				</div>
			</div>

			<IfSet var={error}>
				<div className="row mt-1">
					<div className="col">
						Error: {error}
					</div>
				</div>
			</IfSet>

			{prediction && <div className="row mt-1">
				<div className="col">
					<PredictResultComponent result={prediction} />
				</div>
			</div>}

			<PredictFormComponent onPredict={handlePredict} />
		</div>
	);
}
