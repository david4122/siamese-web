import { useState } from "react";

export type PredictFormComponentProps = {
	onPredict: (img1: File, img2: File) => Promise<any>
};

export default function PredictFormComponent(props: PredictFormComponentProps) {

	const [firstImg, setFirstImg] = useState<File | null>(null);
	const [firstImgUrl, setFirstImgUrl] = useState<string | null>(null);
	const [secondImg, setSecondImg] = useState<File | null>(null);
	const [secondImgUrl, setSecondImgUrl] = useState<string | null>(null);
	const [loading, setLoading] = useState<boolean>(false);

	const updateFirstImage = (e: React.ChangeEvent<HTMLInputElement>) => {
		const file = e.target.files?.[0];
		setFirstImg(file ?? null);

		const url = file ? URL.createObjectURL(file) : null;
		setFirstImgUrl(url);
	};

	const updateSecondImage = (e: React.ChangeEvent<HTMLInputElement>) => {
		const file = e.target.files?.[0];
		setSecondImg(file ?? null);

		const url = file ? URL.createObjectURL(file) : null;
		setSecondImgUrl(url);
	};

	const handlePredict = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		if(!firstImg || !secondImg)
			return;

		setLoading(true);
		props.onPredict(firstImg, secondImg)
			.finally(() => setLoading(false));
	};

	const valid = !!firstImg && !!secondImg;

	return (
		<form className="d-flex flex-column gap-3" onSubmit={handlePredict}>
			<div className="row">
				<div className="col">
					<label htmlFor="formFile" className="form-label">First image</label>
					<input
						className="form-control"
						type="file"
						id="formFile"
						accept="image/png,image/jpeg"
						onInput={updateFirstImage}
						disabled={loading} />
				</div>

				<div className="col">
					<label htmlFor="formFile" className="form-label">Second image</label>
					<input
						className="form-control"
						type="file"
						accept="image/png,image/jpeg"
						onInput={updateSecondImage}
						disabled={loading} />
				</div>
			</div>

			<div className="row">
				<div className="col">
					{firstImgUrl && <img className="w-100" alt="" src={firstImgUrl} />}
				</div>

				<div className="col">
					{secondImgUrl && <img className="w-100" alt="" src={secondImgUrl} />}
				</div>
			</div>

			<div className="row">
				<div className="col">
					<input
						className="btn btn-primary"
						type="submit"
						value="Predict"
						disabled={!valid || loading} />
				</div>
			</div>
		</form>
	);
}
