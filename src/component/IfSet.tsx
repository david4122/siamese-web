import { ReactElement } from "react";

export type IfSetProps = {
	var: any,
	children: ReactElement,
};

export default function IfSet(props: IfSetProps) {
	if(!props.var)
		return <></>;

	return (props.children);
}
