import PredictionResult from "../domain/predictionResult";

export type PredictionResultComponentProps = {
	result: PredictionResult | null,
};

export default function PredictResultComponent(props: PredictionResultComponentProps) {

	if(!props.result)
		return <div></div>;

	return (
		<div className="card">
			<div className="card-body">
				<h5 className="card-title">
					Model: <span className="fw-bold">{props.result.model}</span>
				</h5>
				<div className="card-text">
					Prediction: <span className="fw-bold">{props.result.prediction}</span>
				</div>
			</div>
		</div>
	);
}
