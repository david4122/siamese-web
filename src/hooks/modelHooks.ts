import { useCallback, useEffect, useState } from "react";
import { Model } from "../domain/model";
import env from '../env';
import { okOrReject } from "../utils";
import PredictionResult from "../domain/predictionResult";

type ModelData = {
    name: string,
    train_dataset: string,
    train_loss: number,
    train_accuracy: number,
    test_loss: number,
    test_accuracy: number,
};

export function useModelList() {
	const [models, setModels] = useState<Model[] | null>(null);
	const [error, setError] = useState<string | null>(null);

	useEffect(() => {
		fetch(`${env.apiBaseUrl}/models`)
			.then(okOrReject)
			.then(r => r.json())
			.then(models => setModels(models))
			.catch(err => setError(err));
	}, []);

	return { models, error };
}

export function useModel(name: string | undefined) {
	const [model, setModel] = useState<Model | null>(null);
	const [error, setError] = useState<string | null>(null);

	const graphUrl = (model: string, graph: string) => `${env.apiBaseUrl}/models/${model}/graphs/${graph}`;

	useEffect(() => {
		if(!name)
			return;

		fetch(`${env.apiBaseUrl}/models/${name}`)
			.then(okOrReject)
			.then<ModelData>(r => r.json())
			.then((data: ModelData) => setModel({
				...data,
				model_graph: graphUrl(name, 'model'),
				loss_graph: graphUrl(name, 'loss'),
				accuracy_graph: graphUrl(name, 'accuracy'),
			}))
			.catch(err => setError(err));
	}, [name]);

	return { model, error };
}

export function usePredict(modelName: string | undefined) {
	const [prediction, setPrediction] = useState<PredictionResult | null>(null);
	const [error, setError] = useState<string | null>(null);

	const predict = useCallback((f1: File, f2: File) => {
		setError(null);
		setPrediction(null);

		if(!modelName)
			Promise.reject('Model not set');

		const formData = new FormData();
		formData.append('file_1', f1);
		formData.append('file_2', f2);

		return fetch(`${env.apiBaseUrl}/models/${modelName}/predict`, {
			method: 'POST',
			body: formData,
		})
			.then(okOrReject)
			.then(r => r.json())
			.then(pred => setPrediction(pred))
			.catch(err => setError(err));
	}, [modelName]);

	return { prediction, error, predict };
}

export function usePredictOnAll() {
	const [preds, setPreds] = useState<PredictionResult[] | null>(null);
	const [error, setError] = useState<string | null>(null);

	const predict = useCallback((f1: File, f2: File) => {
		setPreds(null);
		setError(null);

		const formData = new FormData();
		formData.append('file_1', f1);
		formData.append('file_2', f2);

		return fetch(`${env.apiBaseUrl}/models/predict`, {
			method: 'POST',
			body: formData,
		})
			.then(okOrReject)
			.then(r => r.json())
			.then(preds => setPreds(preds))
			.catch(err => setError(err));
	}, []);

	return { predictions: preds, error, predict };
}
