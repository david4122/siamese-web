
export default interface PredictionResult {
	model: string;
	prediction: number;
}
