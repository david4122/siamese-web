
export interface Model {
    name: string;
    train_dataset: string;
    train_loss: number;
    train_accuracy: number;
    test_loss: number;
    test_accuracy: number;
	model_graph: string;
	loss_graph: string;
	accuracy_graph: string;
}
