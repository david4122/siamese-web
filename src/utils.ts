
export function okOrReject(resp: Response) {
	return resp.ok
		? resp
		: resp.text().then(err => Promise.reject(err));
}
